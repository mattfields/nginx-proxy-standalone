# Nginx-proxy standalone

This is a Docker Compose project to run a standalone instance of jwilder's nginx-proxy + letsencrypt-nginx-proxy-companion

## Summary

This is a Docker Compose configuration file to run jwilder's nginx-proxy and letsencrypt companion in docker.

Running this in docker-compose will result in a standalone Nginx proxy running on your local machine, which will automatically pick up new docker containers added to the network, and generate an nginx config for them, as well as run LetsEncrypt SSL certificate generation

## Usage

1.  Start this project in docker compose: `docker-compose up`
2.  Add new sites in docker, using environment variable `VIRTUAL_HOST` for the site name.

        docker run -e VIRTUAL_HOST=foo.bar.com --newtwork=nginx_proxy [some docker image]

3.  Nginx-proxy should pick up the new sites and add them to the network `nginx_proxy`.
    (Watch for output in the nginx_proxy container logs)
4.  You should be able to visit your site url on your local machine, (providing you've added an alias for the host in `/etc/hosts`)

## Security

Note that this container allows access to the docker socket, which is a possible security vulnerability.

You may have to explicity allow access to your docker socket file in your docker config.
(Not covered here.)

Use at your own risk.

## Installation

1. Clone this repo: `git clone git@gitlab.com:mattfields/nginx-proxy-standalone.git`
2. Docker compose build: `docker-compose build`
3. Run project: `docker-compose up`

## Requirements

- docker
- docker-compose (3.6+)

## Project repository

https://gitlab.com/mattfields/nginx-proxy-standalone

## License

MIT

## Credits

Nginx-proxy & letsencrypt nginx-proxy companion:

- https://github.com/nginx-proxy/nginx-proxy
- https://github.com/nginx-proxy/docker-letsencrypt-nginx-proxy-companion
